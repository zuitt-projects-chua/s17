/*alert("Hello")*/

//Common examples of array

let grades = [98.5, 94.3, 89.2, 90.1];

let marvelHeroes = ["Iron Man", "Captain America", "Thor", "Hulk", "Black Widow", "Hawkeye", "Shang Chi", "Spiderman"];

let mixedArray = [12, "Asus", null, undefined, {}];

let myTasks = [
	"drink HTML",
	"eat Javascript",
	"inhale css",
	"bake react"
];

/*
Array and Index

[] array literals 
reassign values in an array
*/

console.log(myTasks);

//changing array values
myTasks[0] = "sleep for 8 hours";
console.log(myTasks);

//accessing array values
console.log(grades[2]);
console.log(marvelHeroes[6]);
console.log(myTasks[20]);
//undefined -  value doesnt exist

console.log(marvelHeroes.length);

if (marvelHeroes.length > 5){
	console.log("We have too many heroes, please contact Thanos")
};

//accessing the last element counting from -1
let lastElement = marvelHeroes.length-2;
console.log(marvelHeroes[lastElement]);

/*
Array Methods 

Mutator Methods change the array after they're created

*/

let fruits = ["Apple", "Blueberry", "Orange", "Grapes"];
//push adds an element in the end of an array 
console.log(fruits);
let fruitsLength = fruits.push("Mango");
console.log(fruits)

//push multiple elements

fruits.push("Guava", "Kiwi");
console.log(fruits);

//pop removes element

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log(fruits);

fruits.pop();
console.log(fruits);

fruits.unshift("Guava");
console.log(fruits);

//unshift adds element at the beginning of array
//uses: display new users at the begining 

fruits.unshift("Kiwi", "Lime");
console.log(fruits);

//shift removes an element at the begining of the array

fruits.shift();
console.log(fruits);

//unshift pop push shift are used for single element manipulation

/*
Splice removes elements from a specified index number and adds the elemnts
Syntax arrayname.splice(startingIndex,deletecount,elements to be added)

*/

fruits.splice(1,2,"Cherry", "Watermelon");
console.log(fruits)

//delete count and replacement are optional

fruits.splice(3);
console.log(fruits);
//removes element from 3 including index 3 is deleted 

//adding elements in a specific index
fruits.splice(2,0, "Cherry", "Buko");
console.log(fruits);

// sort rearranges the array elements in alphabetical order

fruits.sort();
console.log(fruits);

//reverse reverses the order of the elements

fruits.reverse();
console.log(fruits);

/*
Non Mutator Methods

methods do not change or modify array elements
*/

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"]

//indexOf("<element>") returns the index number of the first matching element found in array

console.log(countries.indexOf("PH"));

let indexOfSG = countries.indexOf("SG");
console.log(indexOfSG);

let invalidCountry = countries.indexOf("SK");
console.log(invalidCountry);

//non existing elements returns -1 value

/*
Slice method
slices array and returns a new array
arrayname.slice(startngIndex);
arrayname.slice(startingIndex, ending index);

*/

let slicedArrayA = countries.slice(2);
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2,4);
console.log(slicedArrayB);

let slicedArrayC = countries.slice(-3);
console.log(slicedArrayC);

/*
toString
returns an array, as string separated by comma
*/

let stringArray = countries.toString();
console.log(stringArray);
//displayed as string

let sentence = ["I", "like", "Javascript", "It's", "fun!"]

let sentenceString = sentence.toString();
console.log(sentenceString);

/*
Concat
combines two arrays and returns a combined array
*/

let tasksArrayA = ["drink html", "eat javascript"];
let tasksArrayB = ["inhale css", "breath react"];
let tasksArrayC = ["getgit", "be node!"];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log(tasks);

/*
Combining multiple arrays
*/

let allTasks = tasksArrayA.concat(tasksArrayB,tasksArrayC);
console.log(allTasks);

//combining array with element

let combinedTasks = tasksArrayA.concat("smell express", "throw react");
console.log(combinedTasks);

//join()
/*
array.join("<separator>")
array.join('<separator>')
*/


let members = ["Rose", "Lisa", "Jisoo", "Jennie"];
//comma as default separator
let joinedmembers1 = members.join();
console.log(joinedmembers1);

let joinedmembers2 = members.join("+");
console.log(joinedmembers2);

let joinedmembers3 = members.join("~");
console.log(joinedmembers3);

members.forEach(function(member){
	if (member === "Rose"){
		members.shift()
	}
})
console.log(`New members ${members}`);


/*
Iteration Methods
useful for manipulating array data
*/
//forEach

/*allTasks.forEach(function(tasks){
	console.log(tasks);
});*/

let filteredTasks = [];
allTasks.forEach(function(tasks){
	console.log(tasks);
	
	if (tasks.length > 10 ){
		filteredTasks.push(tasks);
	}
});
console.log(filteredTasks)


/*
map iterates each element with different values
use return keyword
*/

let numbers = [1,2,3,4,5];
let numbersMap = numbers.map(function(number){
	return number * number;
});

console.log(numbersMap);

/*
every() returns a boolean, checks if all elements meet a certain condition
*/

let allValid = numbers.every(function(number){
	return (number < 3);
});
console.log(allValid);

//some( returns if at least one element is true)

let someValid = numbers.some(function(number){
	return (number < 2);
});
console.log(someValid);

let filterValid = numbers.filter(function(number){
	return (number < 3);
});
console.log(filterValid);

/*
includes methods

*/

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"]

let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes("a");
});
console.log(filteredProducts);

/*
Multi dimensional array useful for storing complex data structure
*/

let chessboard = [
	["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
	["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
	["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
	["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
	["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
	["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
	["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
	["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],

];

console.log(chessboard[1][4]);
console.log(chessboard[1])
console.log(chessboard.indexOf("b6"));